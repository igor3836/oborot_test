<?php


class UserRepository
{
    private $pdo;

    public function __construct()
    {
        $this->pdo = Database::connect('database', 'localhost', 'admin', 'admin');
    }

    public function getUsersByName($user_name)
    {
        $stmt = $this->pdo->prepare("SELECT * FROM users WHERE status ='a' AND  user_name=:user_name");
        $stmt->execute(['user_name' => $user_name]);

        return $stmt->fetchAll();
    }

    public function getUsers_by_name($user_name)
    {
        return $this->getUsersByName($user_name);
    }

    public function getUsers($email, $user_name, $user_id)
    {
        $stmt = $this->pdo->prepare(
            <<<SQL
SELECT *
FROM users
WHERE status = 'a'
  AND (
    email=:email
    OR user_name=:user_name
    OR user_id=:user_id
  )
SQL
        );
        $stmt->execute([
            'email' => $email,
            'user_name' => $user_name,
            'user_id' => $user_id
        ]);

        return $stmt->fetchAll();
    }
}

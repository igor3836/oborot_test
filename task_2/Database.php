<?php


class Database
{
    public static function connect($dbname, $host, $user, $password)
    {
        try {
            $pdo = new PDO(sprintf('mysql:dbname=%s;host=%s', $dbname, $host), $user, $password);
        } catch (Exception $exception) {
            die($exception->getMessage());
        }

        return $pdo;
    }
}
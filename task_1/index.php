<?php
require_once 'vendor/autoload.php';

$manager = new DataManager();
$manager->deployDatabase();

$manager->seedData();

echo json_encode($manager->getUsersWithMemos());
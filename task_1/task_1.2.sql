SELECT m.id
FROM memos as m
       LEFT JOIN (SELECT count(id) as item_count, title FROM memos GROUP BY title) AS t
                 ON m.title = t.title
WHERE item_count > 1;
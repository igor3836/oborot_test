<?php


class MemoRepository
{
    private $pdo;

    public function __construct()
    {
        $this->pdo = SQLiteConnector::connect();
    }

    /**
     * @param array $fields
     *
     * @return string
     * @throws InvalidArgumentException
     */
    public function createMemo($fields)
    {
        if (!isset($fields['user_id'], $fields['title'], $fields['content'])) {
            throw new InvalidArgumentException('Не указаны необходимые поля');
        }

        try {
            $stmt = $this->pdo->prepare(
                'INSERT INTO memos (user_id, title, content) VALUES (:user_id, :title, :content)'
            );
            $stmt->execute([
                'user_id' => $fields['user_id'],
                'title' => $fields['title'],
                'content' => $fields['content']
            ]);

            return $this->pdo->lastInsertId();
        } catch (Exception $exception) {
            die($exception->getMessage());
        }
    }

    public function getUserMemos($user_id)
    {
        $stmt = $this->pdo->prepare('SELECT * FROM memos WHERE user_id=:user_id');
        $stmt->execute(['user_id' => $user_id]);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}
<?php


class SQLiteConnector
{
    private static $pdo;

    public static function connect()
    {
        if (!self::$pdo) {
            try {
                self::$pdo = new PDO('sqlite:' . DB_PATH);
                self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (Exception $exception) {
                die($exception->getMessage());
            }
        }

        return self::$pdo;
    }
}
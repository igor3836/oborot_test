<?php


class UserRepository
{
    private $pdo;

    public function __construct()
    {
        $this->pdo = SQLiteConnector::connect();
    }

    /**
     * @param array $fields
     *
     * @return string
     * @throws InvalidArgumentException
     */
    public function createUser($fields)
    {
        if (!isset($fields['name'], $fields['email'])) {
            throw new InvalidArgumentException('Не указаны необходимые поля');
        }

        try {
            $stmt = $this->pdo->prepare('INSERT INTO users (name, email) VALUES (:name, :email)');
            $stmt->execute([
                'name' => $fields['name'],
                'email' => $fields['email']
            ]);

            return $this->pdo->lastInsertId();
        } catch (Exception $exception) {
            die($exception->getMessage());
        }
    }

    public function getAllUsers()
    {
        try {
            $data = $this->pdo->query(
                <<<SQL
SELECT u.*, COUNT(m.id) AS memo_count
FROM users u
    LEFT JOIN memos m ON u.id = m.user_id
GROUP BY u.id
SQL
            );
        } catch (Exception $exception) {
            die($exception->getMessage());
        }

        return $data->fetchAll(PDO::FETCH_ASSOC);
    }
}
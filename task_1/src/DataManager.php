<?php


class DataManager
{
    private $pdo;
    private $faker;
    private $userRepository;
    private $memoRepository;

    const REQUIRED_TABLES = [
        'users' => '
            CREATE TABLE IF NOT EXISTS users (
              id INTEGER PRIMARY KEY ,
              name    VARCHAR(255) NOT NULL,
              email   VARCHAR(255) NOT NULL
            )',
        'notes' => '
            CREATE TABLE IF NOT EXISTS memos (
              id     INTEGER PRIMARY KEY,
              user_id     INTEGER NOT NULL,
              title       VARCHAR(255) NOT NULL,
              content     TEXT NOT NULL,
              date_create DATETIME DEFAULT CURRENT_TIMESTAMP,
              FOREIGN KEY (user_id)
                REFERENCES users (id)
                ON UPDATE NO ACTION ON DELETE CASCADE
            )'
    ];

    public function __construct()
    {
        $this->pdo = SQLiteConnector::connect();
        $this->faker = Faker\Factory::create();
        $this->userRepository = new UserRepository();
        $this->memoRepository = new MemoRepository();
    }

    private function checkDatabaseDeployed()
    {
        try {
            $stmt = $this->pdo->prepare("SELECT name FROM sqlite_master WHERE type='table' AND name=:table_name");
        } catch (Exception $exception) {
            die($exception->getMessage());
        }

        foreach (self::REQUIRED_TABLES as $table_name => $table_query) {
            $stmt->execute(['table_name' => $table_name]);
            if ($stmt->fetchColumn()) {
                continue;
            }

            return false;
        }

        return true;
    }

    public function deployDatabase()
    {
        if ($this->checkDatabaseDeployed()) {
            return;
        }

        try {
            foreach (self::REQUIRED_TABLES as $table_query) {
                $this->pdo->exec($table_query);
            }
        } catch (Exception $exception) {
            die($exception->getMessage());
        }
    }

    public function seedData()
    {
        $this->pdo->exec('DELETE FROM memos');
        $this->pdo->exec('DELETE FROM users');

        $users_count = $this->faker->numberBetween(1, 5);
        for ($i = 0; $i < $users_count; $i++) {
            $this->seedUser();
        }
    }

    private function seedUser()
    {
        $user_id = $this->userRepository->createUser([
            'name' => $this->faker->name,
            'email' => $this->faker->email
        ]);

        // генерируем только три возможных варианта заголовка заметок, чтобы выполнить задание 1.2
        $sentences = [];
        for ($i = 0; $i < 3; $i++) {
            $sentences[] = $this->faker->sentence(3);
        }

        $notes_count = $this->faker->numberBetween(5, 10);
        for ($i = 0; $i < $notes_count; $i++) {
            $this->memoRepository->createMemo([
                'user_id' => $user_id,
                'title' => $this->faker->randomElement($sentences),
                'content' => $this->faker->text
            ]);
        }

        return $user_id;
    }

    public function getUsersWithMemos()
    {
        $users = $this->userRepository->getAllUsers();

        foreach ($users as &$user) {
            $user['memo_list'] = $this->memoRepository->getUserMemos($user['id']);
        }
        unset($user);

        return $users;
    }
}